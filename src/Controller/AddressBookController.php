<?php

namespace App\Controller;

use App\Entity\AddressBook;
use App\Form\AddressBookType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AddressBookController
 * @package App\Controller
 *
 * @Route(
 *     path =   "/",
 *     name =   "address_book_"
 * )
 */
class AddressBookController extends Controller
{
    /**
     * @Route(
     *     name     =   "list",
     *     path     =   "/",
     *     methods  =   {"GET"},
     * )
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $addressBooks = $em->getRepository(AddressBook::class)->findAll();

        return $this->render('AddressBook/list.html.twig', [
            'addressBooks' => $addressBooks
        ]);
    }

    /**
     *
     * @Route(
     *     name     =   "new",
     *     path     =   "/new",
     *     methods  =   {"GET", "POST"},
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $addressBook = new AddressBook();
        $form = $this->createForm(AddressBookType::class, $addressBook, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($addressBook);
            $em->flush();

            $this->addFlash('success', 'Success');

            return $this->redirectToRoute('address_book_list');
        }

        return $this->render('AddressBook/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *
     * @Route(
     *     name     =   "edit",
     *     path     =   "/{id}/edit",
     *     methods  =   {"GET", "POST"},
     * )
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $addressBook = $em->getRepository(AddressBook::class)->find($id);
        $form = $this->createForm(AddressBookType::class, $addressBook, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($addressBook);
            $em->flush();

            $this->addFlash('success', 'Success');

            return $this->redirectToRoute('address_book_list');
        }

        return $this->render('AddressBook/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *
     * @Route(
     *     name     =   "delete",
     *     path     =   "/{id}/delete",
     *     methods  =   {"GET"},
     * )
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $addressBook = $em->getRepository(AddressBook::class)->find($id);

        $em->remove($addressBook);
        $em->flush();

        $this->addFlash('success', 'Success');

        return $this->redirectToRoute('address_book_list');
    }

}
